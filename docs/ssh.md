# Generating the SSH RSA keys
In order to access your account for the **first** time, a **public** SSH RSA key must be sent to the cluster administrator.
Password based access is permanently disabled!
Public key (example filename: `id_rsa.pub`) is always generated together with private key (example filename: `id_rsa`)
The private key must be protected and must **not** be shared with anyone.
Below you can find instruction on how to generate such pair of keys under Linux and Windows operating systems.

## GNU/Linux
One can use `ssh-keygen` command to generate pair of keys under GNU/Linux distribution.
To do so, you need:
* open terminal emulator
* type in `ssh-keygen` and press `Enter`
* you will be asked to type in a password to protect you private key (`Enter passphrase`). This is optional step, but it is recommended.
Here you can see example output:
```
> ssh-keygen 
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/user/.ssh/id_rsa.
Your public key has been saved in /home/user/.ssh/id_rsa.pub.
The key fingerprint is:
43:c5:5b:5f:b1:f1:50:43:ad:20:a6:92:6a:1f:9a:3a
```

* Afterwards, you should find your keys in `~/.ssh` directory:
    * `id_rsa` - this is your private key, **do not share it with anybody**
    * `id_rsa.pub` - this is your public keys, which you should send to the cluster administrator (admin.coimk@ujk.edu.pl)


## Windows (10) with puttygen

* Install the latest full version of putty that is appropriate for your CPU architecture (e.g.: [putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)). At the time of writing the manual, it is version 0.75.
* In the directory where putty is installed (usually: C:\Program Files\PuTTY) launch the program puttygen.exe.
* Make sure the key type is RSA.


![](/doc/img/puttygen_1.png)
![](/img/puttygen_1.png)

* Click the button `Generate`

![](/doc/img/puttygen_2.png)
![](/img/puttygen_2.png)

* When generating the key, move the mouse to make the process random.
* After generating the keys, additionally secure the keys by entering a password in the field `Key passphrase`; repeat the same password in the field `Confirm passphrase`.

![](/doc/img/puttygen_3.png)
![](/img/puttygen_3.png)

* Write down the keys (public and private) in a safe place.

![](/doc/img/puttygen_4.png)
![](/img/puttygen_4.png)

* Send the public key to the system administrator (admin.coimk@ujk.edu.pl).
* Keep the private key **only for yourself**.
* When connecting to the access server using the putty program, indicate to putty the location of the private key using the `Private key file for authentication` field in the SSH -> Auth tab.


![](/doc/img/puttygen_5.png)
![](/img/puttygen_5.png)


