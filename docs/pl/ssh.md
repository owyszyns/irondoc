# Klucze SSH
W celu uzyskania dostępu do konta, należy przesłać publiczny klucz ssh, który będzie używany do logowania. Dostęp odbywa się jedynie przy wykorzystaniu kluczy SSH, 
logowanie przy użyciu hasła jest trwale wyłączone.
Klucz publiczny (np. `id_rsa.pub`) jest zawsze sparowany z kluczem prywatnym (np. `id_rsa`) i są generowane jednocześnie.
Klucz prywatny należy chronić przed niepowołanym dostępem, w szczególności **nie** należy go nikomu wysyłać!
Poniżej zamieszczone są instrukcje generowania kluczy dla systemów operacyjnych Linux i Windows.

## Linux
Do wygenerowania klucza należy użyć komendy `ssh-keygen`.
W tym celu:

* otwórz terminal
* wpisz `ssh-keygen` i naciśnij Enter
* W pewnym momencie zostaniesz poproszeny, o wprowadzenie hasła (linijka `Enter passphrase`), które będzie zabezpieczać twój klucz prywatny przed niepowołanym użyciem. Zalecamy je podać, gdyż to zwiększa bezpieczeństwo. Poniżej przykład wykonania polecenia `ssh-keygen`:
```
> ssh-keygen 
Generating public/private rsa key pair.
Enter file in which to save the key (/home/użytkownik/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/użytkownik/.ssh/id_rsa.
Your public key has been saved in /home/użytkownik/.ssh/id_rsa.pub.
The key fingerprint is:
43:c5:5b:5f:b1:f1:50:43:ad:20:a6:92:6a:1f:9a:3a
```

* Po wykonaniu tych poleceń pojawią się w katalogu `~/.ssh` następujące pliki: 
    * `id_rsa` - jest to klucz prywatny.  **Nie można go zgubić ani nikomu przekazywać**.
    * `id_rsa.pub` - jest to klucz publiczny, który należy przesłać do administratora na adres admin.coimk@ujk.edu.pl

## Windows (puttygen)

* Zainstaluj najnowszą pełną wersję programu [putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html), właściwą dla architektury używanego procesora. W momencie pisania podręcznika jest to wersja 0.75.
* W katalogu z zainstalowaną wersją putty (zwykle: C:\Program Files\PuTTY) uruchom program puttygen.exe.
* Upewnij się, że wybrany typ klucza to RSA.

![](/doc/img/puttygen_1.png)
![](/img/puttygen_1.png)

* Kliknij przycisk `Generate`

![](/doc/img/puttygen_2.png)
![](/img/puttygen_2.png)

* W trakcie generowania klucza poruszaj myszą aby zapewnić losowość procesu.
* Po wygenerowaniu kluczy zabezpiecz dodatkowo klucze wstawiając hasło w polu `Key passphrase`; powtórz to samo hasło w polu `Confirm passphrase`.

![](/doc/img/puttygen_3.png)
![](/img/puttygen_3.png)

* Klucze (publiczny i prywatny) zapisz w bezpiecznym miejscu.

![](/doc/img/puttygen_4.png)
![](/img/puttygen_4.png)

* Klucz publiczny wyślij administratorowi admin.coimk@ujk.edu.pl
* Klucz prywatny zachowaj **tylko dla siebie**.
* W momencie łączenia się z serwerem dostępowym za pomocą programu putty należy wskazać programowi putty lokalizację klucza prywatnego używając pola `Private key file for authentication` w zakładce `SSH->Auth`

![](/doc/img/puttygen_5.png)
![](/img/puttygen_5.png)


