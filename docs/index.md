# How to get and account 

* Read the user [rules](/docs/RegulaminUzytkownika.pdf) (Polish version only)
* Fill the [registration form](/docs/wniosek_v1.pdf) (Polish version only)
* Send scan of filled and signed form to coimk@ujk.edu.pl
* Wait for approval.
* Send approved form to admin.coimk@ujk.edu.pl and your public SSH RSA key. You can find help [here](ssh).
* Be patient. Depending on requirements specified in the form, it may take few days to create an account.
  

